import { AuthComponent } from './containers/auth/auth.component';
import { AuthGuard } from './core/services/auth/auth.guard';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HomeComponent } from './containers/home/home.component';
import { ItemsComponent } from './containers/items/components/items/items.component';
import { ItemDetailsComponent } from './containers/items/components/item-details/item-details.component';
import { WorkersComponent } from './containers/workers/components/workers/workers.component';
import { RegisterComponent } from './containers/register/components/register/register.component';
import { ProfileComponent } from './containers/profile/components/profile/profile.component';
import { ItemDetailsResolver } from './containers/items/components/item-details/items-details.resolver';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent },
      { path: 'items', component: ItemsComponent },
      { path: 'items/:id', component: ItemDetailsComponent, resolve: { item: ItemDetailsResolver } },
      { path: 'workers', component: WorkersComponent },
      { path: 'register', component: RegisterComponent },
      { path: 'login', component: AuthComponent },
      { path: 'profile', canActivate: [AuthGuard], component: ProfileComponent },
      { path: '**', redirectTo: '/' }
    ])
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
