
export interface Profile {
  username: string,
  birthDate: string,
  hobbies: string[]
}

export interface ProfileResp {
  _id: string,
  username: string,
  birthDate: string,
  hobbies: string[]
}
