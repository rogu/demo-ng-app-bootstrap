import { ProfileResp } from './../profile.models';
import { Api } from './../../../shared/utils/api';
import { HttpServiceModel, ResponseData } from '../../../shared/services/http.models';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class ProfileService implements HttpServiceModel<ProfileResp> {

  constructor(private http: HttpClient) { }

  fetch(filters?: any): Observable<ResponseData<ProfileResp>> {
    return this.http.get<ResponseData<ProfileResp>>(Api.AUTH_PROFILE);
  }

  add(item: any): Observable<any> {
    throw new Error("Method not implemented.");
  }

  update(item: any): Observable<any> {
    throw new Error("Method not implemented.");
  }

  remove(id: any): Observable<any> {
    throw new Error("Method not implemented.");
  }

  get(id: string): Observable<any> {
    throw new Error("Method not implemented.");
  }

}
