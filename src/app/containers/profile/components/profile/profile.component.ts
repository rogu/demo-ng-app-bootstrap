import { Profile } from './../../profile.models';
import { DataGridRowConfig, FieldTypes } from '../../../../shared/components/data-grid/data-grid.models';
import { Component, AfterViewInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ProfileService } from '../../services/profile.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html'
})
export class ProfileComponent implements AfterViewInit {
  profile$!: Observable<Profile>;
  config: DataGridRowConfig<string>[] = [
    { key: 'key' },
    { key: 'value', type: FieldTypes.INPUT, disabled: true }
  ];

  constructor(private profileService: ProfileService) { }

  ngAfterViewInit(): void {
    this.profile$ = this.profileService.fetch().pipe(
      map((resp) => {
        const { _id, ...rest } = resp.data;
        return {
          ...rest,
          birthDate: new Date(+rest.birthDate).toLocaleDateString(),
          hobbies: rest.hobbies && Object.entries(rest.hobbies).filter(([, value]) => value).map(([hobbies]) => hobbies)
        }
      }));
  }

}
