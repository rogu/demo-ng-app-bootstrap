import { RouterModule } from '@angular/router';
import { AppRoutingModule } from '../app-routing.module';
import { HomeComponent } from './home/home.component';
import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { ItemsComponent } from "./items/components/items/items.component";
import { ItemDetailsComponent } from "./items/components/item-details/item-details.component";
import { RegisterComponent } from "./register/components/register/register.component";
import { WorkersComponent } from "./workers/components/workers/workers.component";
import { SharedModule } from "../shared/shared.module";
import { ProfileComponent } from "./profile/components/profile/profile.component";
import { ModalAddItemComponent } from './items/components/add/modal-add-item.component';
import { AddWorkerComponent } from './workers/components/add-worker/add-worker.component';

@NgModule({
  declarations: [
    HomeComponent,
    ItemsComponent,
    ModalAddItemComponent,
    ItemDetailsComponent,
    RegisterComponent,
    WorkersComponent,
    AddWorkerComponent,
    ProfileComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    NgbModule,
    RouterModule
  ]
})

export class ContainersModule {

}
