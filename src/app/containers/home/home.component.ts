declare var require: any;
const { version } = require('../../../../package.json');
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {
  version: any;

  constructor() {
    this.version = version;
  }

  ngOnInit() {

  }

}
