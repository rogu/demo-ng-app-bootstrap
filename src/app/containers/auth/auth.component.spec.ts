import { Api } from '../../shared/utils/api';
import { CoreService } from '../../shared/services/core.service';
import { RouterTestingModule } from '@angular/router/testing';
import { AuthService } from '../../core/services/auth/auth.service';
import { ComponentFixture, TestBed } from "@angular/core/testing";
import { AuthComponent } from "./auth.component";
import { FormsModule } from "@angular/forms";
import { DebugElement } from "@angular/core";
import { of } from "rxjs";
import { HttpClientTestingModule, HttpTestingController, TestRequest } from "@angular/common/http/testing";
import { By } from "@angular/platform-browser";


describe('auth component', () => {

  let coreService: CoreService,
    fixture: ComponentFixture<AuthComponent>,
    instance: AuthComponent,
    element: DebugElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        AuthComponent
      ],
      imports: [
        FormsModule,
        HttpClientTestingModule,
        RouterTestingModule
      ],
      providers: [
        AuthService,
        CoreService
      ]
    });

    fixture = TestBed.createComponent(AuthComponent);
    instance = fixture.componentInstance;
    element = fixture.debugElement;
    coreService = TestBed.get(CoreService);

  });

  describe('login form', () => {

    it('should be display after detect changes', () => {
      fixture.detectChanges();
      const loginForm = element.query(By.css('form'));
      expect(loginForm).toBeTruthy();
    });

    it('should not be display after success login', () => {
      const authService = TestBed.get(AuthService)
      spyOn(authService, 'logIn').and.returnValue(of({ ok: true }));
      instance.logIn({});
      expect(authService.logIn).toHaveBeenCalled();

      fixture.detectChanges();
      const loginForm = element.query(By.css('form'));
      expect(loginForm).toBeFalsy();
    });

  });

  describe('coreService.isLogged ', () => {

    let req: TestRequest;

    beforeEach(() => {
      const http = TestBed.get(HttpTestingController);
      instance.logIn({});
      req = http.expectOne(Api.AUTH_LOGIN);
    });

    it('should be true', () => {
      req.flush({ ok: true });
      expect(coreService.access$.getValue()).toBeTruthy();
    });

  });

});
