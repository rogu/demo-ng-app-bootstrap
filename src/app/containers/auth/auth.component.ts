import { CoreService } from '../../shared/services/core.service';
import { ResponseAuth } from '../../core/services/auth/auth.service.models';
import { AuthService } from '../../core/services/auth/auth.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['auth.component.scss']
})
export class AuthComponent {

  constructor(
    private authService: AuthService,
    private coreService: CoreService
  ) { }

  get access$() {
    return this.coreService.access$;
  }

  logIn(formValue: any) {
    this.authService
      .logIn(formValue)
      .subscribe(({ accessToken, message }: ResponseAuth) => {
        this.coreService.setToken(accessToken, message);
      });
  }

  logOut() {
    this.authService.logOut();
  }

}
