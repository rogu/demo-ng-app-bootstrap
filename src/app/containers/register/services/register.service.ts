import { ResponseAuth } from '../../../core/services/auth/auth.service.models';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Api } from "../../../shared/utils/api";

@Injectable({ providedIn: 'root' })
export class RegisterService {

  constructor(private http: HttpClient) { }

  register(user: any): Observable<any> {
    return this.http.post<ResponseAuth>(Api.AUTH_REGISTER, user);
  }

}
