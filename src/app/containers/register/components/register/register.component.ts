import { ResponseData } from '../../../../shared/services/http.models';
import { CustomValidators } from './../../../../shared/utils/custom-validators';
import { CustomServerValidatorsService } from '../../../../shared/services/custom-server-validators.service';
import { FormArray, FormGroupDirective, NgForm } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { RegisterService } from '../../services/register.service';
import { Api } from '../../../../shared/utils/api';
import { catchError } from 'rxjs/operators';
import { throwError, Observable } from 'rxjs';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm!: FormGroup;

  constructor(private registerService: RegisterService,
    private customServerValidators: CustomServerValidatorsService) { }

  ngOnInit() {
    this.registerForm = new FormGroup({
      username: new FormControl('', [
        Validators.required,
        Validators.email
      ], (control: any): Observable<any> =>
        this.customServerValidators.doesItExist(Api.AUTH_CHECK_USERNAME, 'username', control)),
      passwordGroup: new FormGroup({
        password: new FormControl('', [
          Validators.required,
          CustomValidators.password
        ]),
        confirmPassword: new FormControl('')
      }, CustomValidators.equalRequired),
      birthDate: new FormControl('', [
        Validators.required,
        CustomValidators.dateShouldBeLessThan('2021-01-01')
      ]),
      hobbies: new FormGroup({
        tv: new FormControl(''),
        music: new FormControl(''),
        sport: new FormControl(''),
        travel: new FormControl(''),
      }, CustomValidators.atLeastOneShouldBeSelected),
      specialRequests: new FormArray([
        new FormControl(null)
      ])
    });
  }

  onAddSpecialRequest() {
    (this.registerForm.controls.specialRequests as FormArray).push(new FormControl(null));
  }

  track(idx: number) {
    return idx;
  }

  sendForm(ngForm: FormGroupDirective) {
    if (this.registerForm.valid) {
      this.registerService
        .register({
          ...this.registerForm.value,
          password: this.registerForm.value.passwordGroup.password,
          birthDate: Date.parse(Object.values(this.registerForm.value.birthDate).join('-')).toString()
        })
        .pipe(
          catchError((error) => {
            alert(JSON.stringify(error));
            return throwError(() => error);
          })
        )
        .subscribe(({ message }: ResponseData<any>) => {
          alert(message);
          ngForm.resetForm();
          this.registerForm.reset();
        });
    } else {
      console.error('form invalid');
    }
  }

}
