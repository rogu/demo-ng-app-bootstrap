import { ModalBase } from './../../../../shared/components/modal/modal-base';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-add-worker',
  templateUrl: './add-worker.component.html'
})
export class AddWorkerComponent extends ModalBase implements OnInit {

  @Output() onChange = new EventEmitter();
  workerForm!: FormGroup;

  constructor(modalService: NgbModal) {
    super(modalService);
  }

  ngOnInit(): void {
    this.workerForm = new FormGroup({
      name: new FormControl('', Validators.required),
      phone: new FormControl('', Validators.required),
      category: new FormControl('', Validators.required)
    });
  }

  send(data: any) {
    this.onChange.emit({ type: 'add', data });
    this.workerForm.reset();
  }

}
