import { GridAction } from './../../../../shared/components/data-grid/data-grid.models';
import { WorkerKeys } from '../../services/workers.models';
import { SearchControlModel } from '../../../../shared/components/search/search.model';
import { DataGridRowConfig } from '../../../../shared/components/data-grid/data-grid.models';
import { WorkersGateService } from '../../services/workers-gate.service';
import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FieldTypes as SearchFieldTypes } from '../../../../shared/components/search/search.model';
import { FieldTypes as DataGridFieldTypes } from '../../../../shared/components/data-grid/data-grid.models';
import { WorkerModel } from '../../services/workers.models';
import { Observable } from 'rxjs';

@Component({
  templateUrl: './workers.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WorkersComponent implements OnInit {
  searchConfig!: SearchControlModel<WorkerKeys>[];
  dataGridConfig!: DataGridRowConfig<WorkerKeys>[];
  filters$: Observable<any> = this.gateService.filters$;
  workers$: Observable<WorkerModel[] | null> = this.gateService.workers$;

  constructor(
    public gateService: WorkersGateService
  ) { }

  ngOnInit(): void {
    this.searchConfig = [
      { name: 'name', tag: SearchFieldTypes.INPUT_TEXT },
      { name: 'phone', tag: SearchFieldTypes.INPUT_TEXT },
      { name: 'category', tag: SearchFieldTypes.SELECT, value: '', options: ['sales', 'support'] }
    ];

    this.dataGridConfig = [
      { key: 'name' },
      { key: 'phone', type: DataGridFieldTypes.INPUT },
      { type: DataGridFieldTypes.BUTTON, header: 'remove' },
      { type: DataGridFieldTypes.BUTTON, header: 'more' },
    ]
  }

  onGridChange(action: GridAction) {
    switch (action.type) {
      case 'add': this.gateService.add(action.data); break;
      case 'remove': this.gateService.remove(action.data); break;
      case 'update': this.gateService.update(action.data); break;
      case 'more': alert('WORKER DETAILS \n' + JSON.stringify(action.data, null, 4)); break;
    }
  }

  updateFilters(value: {}) {
    this.gateService.updateFilters(value);
  }

}
