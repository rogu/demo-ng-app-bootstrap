import { HttpCRUDService } from './../../../shared/services/http.crud.service';
import { WorkerModel } from './workers.models';
import { Injectable } from '@angular/core';
import { Api } from "../../../shared/utils/api";
import { HttpClient } from "@angular/common/http";

@Injectable({ providedIn: 'root' })
export class WorkersApiService extends HttpCRUDService<WorkerModel> {

    constructor(http: HttpClient) {
        super(http, Api.DATA_WORKERS);
    }

}
