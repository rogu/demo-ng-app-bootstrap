import { BehaviorSubject, Observable } from 'rxjs';
import { WorkerModel, WorkersFilters } from './workers.models';
import { Injectable } from '@angular/core';
import { WorkersApiService } from './workers.api.service';

@Injectable({
  providedIn: 'root'
})
export class WorkersGateService {

  public _workers$: BehaviorSubject<WorkerModel[] | null> = new BehaviorSubject<WorkerModel[] | null>(null);
  private _filters$: BehaviorSubject<WorkersFilters> = new BehaviorSubject({});

  constructor(
    private workerApiService: WorkersApiService
  ) {
    this.fetch();
  }

  fetch() {
    this.workerApiService.fetch().subscribe((resp) => this._workers$.next(resp.data));
  }

  get workers$(): Observable<WorkerModel[] | null> {
    return this._workers$.asObservable();
  }

  get filters$(): Observable<WorkersFilters> {
    return this._filters$.asObservable();
  }

  add(worker: WorkerModel) {
    this.workerApiService
      .add(worker)
      .subscribe(() => this.fetch());
  }

  update(worker: WorkerModel) {
    this.workerApiService
      .update(worker)
      .subscribe();
  }

  updateFilters(value: {}) {
    this._filters$.next({ ...this._filters$.value, ...value });
  }

  remove(worker: WorkerModel) {
    if (confirm('are you sure?')) {
      this.workerApiService
        .remove(worker.id)
        .subscribe(() => this.fetch());
    }
  }

}
