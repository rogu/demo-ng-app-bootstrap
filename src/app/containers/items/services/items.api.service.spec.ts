import { Api } from './../../../shared/utils/api';
import { AuthService } from '../../../core/services/auth/auth.service';
import { inject, TestBed } from '@angular/core/testing';
import { ItemsApiService } from './items.api.service';
import { BehaviorSubject } from "rxjs";
import { HttpClientTestingModule, HttpTestingController } from "@angular/common/http/testing";

describe('itemsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      declarations: [],
      providers: [
        ItemsApiService,
        AuthService
      ]
    });
  });

  it('fetch method should return response', () => {
    const filters = new BehaviorSubject({ title: 'tomato' });
    const expectedReqUrl = Api.DATA_ITEMS + "?title=tomato";

    const itemsService = TestBed.get(ItemsApiService);
    const http = TestBed.get(HttpTestingController);
    // fake response
    const expectedData = [{ title: 'potatoes' }];
    let actualData: any[] = [];

    itemsService
      .fetch(filters.getValue())
      .subscribe((data: any) => {
        actualData = data;
      });

    const req = http.expectOne(expectedReqUrl);
    expect(req.request.method).toEqual('GET');
    req.flush(expectedData);
    expect(actualData).toEqual(expectedData);
    http.verify();
  });

  it('remove method should return {ok:1}', inject([ItemsApiService], (itemsService: any) => {
    const itemId = 123;
    const serverUrl = `${Api.DATA_ITEMS}/${itemId}`;
    const responseBody = { ok: 1 };
    const http = TestBed.get(HttpTestingController);

    itemsService
      .remove(itemId)
      .subscribe((response: any) => expect(response.ok).toBeTruthy());

    const req = http.expectOne(serverUrl);
    expect(req.request.method).toBe('DELETE');
    req.flush(responseBody);
    http.verify();
  }));

});
