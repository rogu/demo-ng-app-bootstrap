import { Api } from './../../../shared/utils/api';
import { HttpCRUDService } from './../../../shared/services/http.crud.service';
import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { ItemModel } from './items.models';

@Injectable({ providedIn: 'root' })
export class ItemsApiService extends HttpCRUDService<ItemModel> {

    constructor(http: HttpClient) {
        super(http, Api.DATA_ITEMS);
    }

}
