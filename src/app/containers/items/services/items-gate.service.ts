import { CoreService } from '../../../shared/services/core.service';
import { ResponseData } from '../../../shared/services/http.models';
import { Observable, BehaviorSubject } from 'rxjs';
import { ItemModel, ItemsFilters, FiltersModel } from './items.models';
import { ItemsApiService } from './items.api.service';
import { Injectable } from '@angular/core';
import { debounceTime } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ItemsGateService {

  private _access$ = this.coreService.access$;
  private _items$: BehaviorSubject<ResponseData<ItemModel> | null> = new BehaviorSubject<ResponseData<ItemModel> | null>(null);
  private _filters$: BehaviorSubject<ItemsFilters> = new BehaviorSubject(new ItemsFilters());

  get access$(): Observable<boolean> {
    return this._access$.asObservable();
  }

  public get items$(): Observable<ResponseData<ItemModel> | null> {
    return this._items$.asObservable();
  }

  get filters$(): Observable<ItemsFilters> {
    return this._filters$.asObservable();
  }

  constructor(
    private itemsApiService: ItemsApiService,
    private coreService: CoreService
  ) {
    this._filters$
      .subscribe(_ => this.fetch());
  }

  updateFilters(value: { [key in keyof FiltersModel]: any }) {
    this._filters$.next({ ...this._filters$.value, ...value });
  }

  fetch() {
    this.itemsApiService
      .fetch(this._filters$.value)
      .subscribe((resp) => this._items$.next(resp));
  }

  add(item: ItemModel): Observable<ResponseData<ItemModel>> {
    return this.itemsApiService.add(item);
  }

  update(item: ItemModel) {
    this.itemsApiService
      .update(item)
      .subscribe();
  }

  remove(item: ItemModel): Observable<ResponseData<ItemModel>> {
    return this.itemsApiService.remove(item.id);
  }
}
