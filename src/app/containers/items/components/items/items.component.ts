import { ItemsFilters } from './../../services/items.models';
import { ItemKeys, FiltersKeys } from '../../services/items.models';
import { ItemsGateService } from './../../services/items-gate.service';
import { Actions, ResponseData } from '../../../../shared/services/http.models';
import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FieldTypes as SearchFieldTypes, SearchControlModel } from '../../../../shared/components/search/search.model';
import { Router } from '@angular/router';
import { FieldTypes as DGFieldTypes, DataGridRowConfig, GridAction } from '../../../../shared/components/data-grid/data-grid.models';
import { Observable } from 'rxjs';
import { ItemModel } from '../../services/items.models';

@Component({
  templateUrl: './items.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ItemsComponent implements OnInit {
  searchConfig!: SearchControlModel<FiltersKeys>[];
  dataGridConfig!: DataGridRowConfig<ItemKeys>[];
  items$: Observable<ResponseData<ItemModel> | null> = this.gateService.items$;
  access$: Observable<boolean> = this.gateService.access$;
  filters$: Observable<ItemsFilters> = this.gateService.filters$;

  constructor(
    public gateService: ItemsGateService,
    public router: Router
  ) { }

  onAction(action: GridAction): void {
    switch (action.type) {
      case Actions.Add:
        this.gateService.add(action.data).subscribe(_ => this.gateService.fetch());
        break;
      case Actions.Remove:
        confirm('are you sure?')
          && this.gateService.remove(action.data).subscribe(_ => this.gateService.fetch());
        break;
      case Actions.Update:
        this.gateService.update(action.data);
        break;
      case Actions.More:
        this.router.navigate(['items', action.data.id]);
        break;
    }
  }

  ngOnInit(): void {
    this.searchConfig = [
      { name: 'title', tag: SearchFieldTypes.INPUT_TEXT },
      { name: 'priceFrom', tag: SearchFieldTypes.INPUT_NUMBER },
      { name: 'category', tag: SearchFieldTypes.SELECT, value: '', options: ['food', 'clothes'] },
      { name: 'itemsPerPage', value: '5', tag: SearchFieldTypes.SELECT, options: ['2', '5', '10'] },
    ]

    this.gateService.access$.subscribe((access) =>
      this.dataGridConfig = [
        { key: 'title' },
        { key: 'price', type: DGFieldTypes.INPUT, disabled: !access },
        { key: 'imgSrc', type: DGFieldTypes.IMAGE },
        { type: DGFieldTypes.BUTTON, disabled: !access, header: 'remove' },
        { type: DGFieldTypes.BUTTON, header: 'more' }
      ]
    )
  }

  updateFilters(filter: any) {
    this.gateService.updateFilters(filter);
  }

}
