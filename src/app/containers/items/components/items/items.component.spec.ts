import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { ItemsComponent } from './items.component';
import { SearchComponent } from '../../../../shared/components/search/search.component';
import { CamelCaseToSignPipe } from '../../../../shared/pipes/camel-case-to-sign/camel-case-to-sign';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { CoreService } from '../../../../shared/services/core.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ItemsApiService } from '../../services/items.api.service';

describe('Items component', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        RouterTestingModule,
        HttpClientTestingModule
      ],
      declarations: [
        ItemsComponent,
        SearchComponent,
        CamelCaseToSignPipe
      ],
      providers: [
        ItemsApiService,
        CoreService
      ]
    });
  });

  describe('when filters change', () => {
    it('should call fetchItems() after 500 ms', fakeAsync(() => {
      TestBed
        .overrideComponent(ItemsComponent, {
          set: {
            template: '<div>Overridden template here</div>'
          }
        });
      const fixture = TestBed.createComponent(ItemsComponent);
      const instance = fixture.componentInstance;
      spyOn(instance.gateService, 'fetch');
      fixture.detectChanges();
      instance.gateService.updateFilters({ itemsPerPage: 20 });
      tick(500);
      expect(instance.gateService.fetch).toHaveBeenCalled();
    }));
  });

  describe('when input in searchComponent changed', () => {
    it('should call fetchItems()', fakeAsync(async () => {
      TestBed
        .overrideComponent(ItemsComponent, {
          set: {
            template: `
                        <app-search
                            [controls]='searchConfig'
                            (searchChange)='updateFilters($event)'>
                        </app-search>`
          }
        });
      const fixture = TestBed.createComponent(ItemsComponent);
      const instance = fixture.componentInstance;
      spyOn(instance.gateService, 'fetch');
      fixture.detectChanges();
      tick(500);
      await fixture.whenStable();
      const searchByTitle = fixture.debugElement.query(By.css('#search-by-title')).nativeElement;
      searchByTitle.value = 'cheese';
      searchByTitle.dispatchEvent(new Event('input'));
      tick(500);
      expect(instance.gateService.fetch).toHaveBeenCalled();
    }));
  });

});
