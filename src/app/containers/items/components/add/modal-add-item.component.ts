import { ModalBase } from './../../../../shared/components/modal/modal-base';
import { CustomServerValidatorsService } from '../../../../shared/services/custom-server-validators.service';
import { Api } from './../../../../shared/utils/api';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { GridAction } from 'src/app/shared/components/data-grid/data-grid.models';

@Component({
  selector: 'modal-add',
  templateUrl: './modal-add-item.component.html',
  providers: [
    CustomServerValidatorsService
  ]
})

export class ModalAddItemComponent extends ModalBase implements OnInit {
  addItemForm!: FormGroup;
  uploadUrl!: string;
  @Output() onChange: EventEmitter<GridAction> = new EventEmitter();

  constructor(modalService: NgbModal, private customServerValidators: CustomServerValidatorsService) {
    super(modalService);
  }

  ngOnInit(): void {
    this.addItemForm = new FormGroup({
      title: new FormControl(null, [
        Validators.required,
        Validators.minLength(3)
      ], (control: any): Observable<any> => this.customServerValidators.doesItExist(Api.DATA_ITEM_EXISTS, 'title', control)),
      imgSrc: new FormControl(null),
      price: new FormControl(null, [
        Validators.required,
        Validators.pattern('[1-9]+[0-9]*')
      ]),
      category: new FormControl('', Validators.required)
    });
    this.uploadUrl = Api.DATA_UPLOAD;
  }

  send(formValue: any) {
    this.onChange.emit({ type: 'add', data: formValue });
    this.addItemForm.reset();
  }
}
