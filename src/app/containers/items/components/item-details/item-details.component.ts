import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Data } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
    selector: 'item-details',
    templateUrl: './item-details.component.html'
})
export class ItemDetailsComponent implements OnInit {

    data$!: Observable<Data>;

    constructor(
        private route: ActivatedRoute
    ) { }

    ngOnInit(): void {
        this.data$ = this.route.data.pipe(map(({ item }) => item));
    }

}
