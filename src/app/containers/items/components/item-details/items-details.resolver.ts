import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { ItemModel, ItemResolver } from '../../services/items.models';
import { ItemsApiService } from '../../services/items.api.service';

@Injectable({ providedIn: 'root' })
export class ItemDetailsResolver implements ItemResolver<ItemModel> {

    constructor(private itemsService: ItemsApiService) { }

    resolve(route: ActivatedRouteSnapshot): Observable<any> {
        return this.itemsService.get(route.params['id'])
    }

}
