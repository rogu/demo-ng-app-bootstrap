import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataGridComponent } from './components/data-grid/data-grid.component';
import { DataGridRowComponent } from './components/data-grid/data-grid-row/data-grid-row.component';
import { TranslatePipe } from './pipes/translate/translate.pipe';
import { SearchPipe } from './pipes/search/search-pipe';
import { SearchComponent } from './components/search/search.component';
import { FileUploadComponent } from './components/upload/file-upload.component';
import { ModalConfirmComponent } from './components/modal/confirm/modal-confirm.component';
import { ErrorsComponent } from './components/errors/errors.component';
import { SetColorDirective } from './directives/set-color.directive';
import { CamelCaseToSignPipe } from './pipes/camel-case-to-sign/camel-case-to-sign';

@NgModule({
    declarations: [
        DataGridComponent,
        DataGridRowComponent,
        SearchComponent,
        FileUploadComponent,
        ModalConfirmComponent,
        ErrorsComponent,
        SetColorDirective,
        CamelCaseToSignPipe,
        SearchPipe,
        TranslatePipe
    ],
    exports: [
        DataGridComponent,
        DataGridRowComponent,
        SearchComponent,
        FileUploadComponent,
        ModalConfirmComponent,
        ErrorsComponent,
        SetColorDirective,
        CamelCaseToSignPipe,
        SearchPipe,
        TranslatePipe
    ],
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule
    ]
})

export class SharedModule { }
