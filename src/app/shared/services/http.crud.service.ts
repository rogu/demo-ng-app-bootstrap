import { ItemModel } from './../../containers/items/services/items.models';
import { ResponseData, HttpServiceModel } from './http.models';
import { WorkerModel } from '../../containers/workers/services/workers.models';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

type types = ItemModel | WorkerModel;

export abstract class HttpCRUDService<T extends types> implements HttpServiceModel<T> {

    http: HttpClient;
    url: string;

    constructor(http: HttpClient, url: string) {
        this.http = http;
        this.url = url;
    }

    fetch(params?:any): Observable<ResponseData<T>> {
        return this.http.get<ResponseData<T>>(this.url, { params });
    }

    add(item: T): Observable<ResponseData<T>> {
        return this.http.post<ResponseData<T>>(this.url, item);
    }

    update(item: T): Observable<ResponseData<T>> {
        return this.http.patch<ResponseData<T>>(`${this.url}/${item.id}`, item);
    }

    remove(id: string): Observable<ResponseData<T>> {
        return this.http.delete<ResponseData<T>>(this.url + "/" + id);
    }

    get(id: string): Observable<T> {
        return this.http.get<ResponseData<T>>(`${this.url}/${id}`).pipe(map((resp: ResponseData<T>) => resp.data))
    }

}
