import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class DictService {
  dict: { [key: string]: { [key: string]: string } } = {
    pl: {
      required: 'pole wymagane',
      minlength: 'tekst za krótki',
      atLeastOne: 'wybierz min. jeden',
      dateShouldBeLessThan: 'data powinna być mniejsza od: ',
      email: 'email niepoprawny',
      password: '4-8 znaków, małe i duże litery oraz cyfry',
      equal: 'wyrażenia nie są zgodne'
    },
    de: {
      required: 'erforderlich',
      minlength: 'min-Länge',
      atLeastOne: 'mindestens ein',
      dateShouldBeLessThan: 'datum sollte kleiner sein als: ',
      email: 'ungültige E-Mail',
      password: '4-8 Zeichen, Buchstaben und Zahlen',
      equal: 'Ausdrücke stimmen nicht überein'
    }
  };
}
