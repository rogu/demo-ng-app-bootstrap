import { AuthService } from '../../core/services/auth/auth.service';
import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
  HttpErrorResponse
} from "@angular/common/http";
import { iif, Observable, throwError } from "rxjs";
import { Injectable } from "@angular/core";
import { tap, catchError, switchMap } from 'rxjs/operators';
import { CoreService } from './core.service';

@Injectable()
export class XhrInterceptor implements HttpInterceptor {

  constructor(
    private coreService: CoreService,
    private authService: AuthService
  ) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.coreService.unsaved$.next(true);
    const reqClone = this.addToken(req, localStorage.getItem('token'));
    return next
      .handle(reqClone)
      .pipe(
        tap(evt => {
          evt instanceof HttpResponse && this.coreService.unsaved$.next(false);
        }),
        catchError(({ status, error: { error, message } }: HttpErrorResponse) => {
          switch (status) {
            case 401:
              return this.refreshToken(reqClone, next);
            default:
              alert(error + '\n' + message);
          }
          return throwError(() => error);
        })
      )
  }

  refreshToken(req: HttpRequest<any>, next: HttpHandler) {
    return this.authService.refreshToken()
      .pipe(
        switchMap(({ accessToken, error }) => iif(() => !!accessToken,
          next.handle(this.addToken(req, accessToken)).pipe(tap(() =>
            this.coreService.setToken(accessToken, 'token ok'))),
          throwError(() => error).pipe(tap(() => this.coreService.setAccess(error, '', false))))
        ),
        catchError(error => {
          this.coreService.setAccess(error, '', false);
          return throwError(() => error);
        })
      )
  }

  addToken(req: HttpRequest<any>, token: string | null) {
    const opt = token ? { setHeaders: { authorization: token } } : {};
    return req.clone({ ...opt });
  }
}
