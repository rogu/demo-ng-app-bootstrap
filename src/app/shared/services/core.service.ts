import { Injectable } from '@angular/core';
import { BehaviorSubject, ReplaySubject, Subject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class CoreService {

  lang: string = 'pl';
  readonly access$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  readonly unsaved$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  readonly serverInfo$: ReplaySubject<{ error: string | null, message: string }> = new ReplaySubject();

  setToken(token: string, message: string) {
    localStorage.setItem('token', token);
    this.serverInfo$.next({ error: null, message });
    this.access$.next(true);
  }

  setAccess(error: string, message: string, value: boolean) {
    this.serverInfo$.next({ error, message });
    this.access$.next(value);
  }

}
