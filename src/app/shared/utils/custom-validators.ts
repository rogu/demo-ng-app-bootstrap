import { AbstractControl, FormGroup, ValidationErrors, ValidatorFn } from "@angular/forms";

export class CustomValidators {
  static password(control: AbstractControl): ValidationErrors | null {
    if (!/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{4,8}$/.test(control.value)) {
      return { password: true };
    }
    return null;
  }

  static equalRequired(group: any): ValidationErrors | null {
    const values = Object.values(group.value);
    if (!values.every((value) => values[0] === value)) {
      return { equal: true };
    }
    return null;
  };

  static atLeastOneShouldBeSelected(group: any): ValidationErrors | null {
    if (Object.values(group.value).some(value => !!value)) {
      return null;
    }
    return { atLeastOne: true };
  };

  static dateShouldBeLessThan(date: string): ValidatorFn {
    return (control: AbstractControl) => {
      const userData = Date.parse(Object.values(control.value).join('-'));
      return userData < Date.parse(date) ? null : { dateShouldBeLessThan: date };
    }
  }
}
