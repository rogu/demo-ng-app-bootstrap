import { environment } from '../../../environments/environment';

export class Api {
    // Data end-points
    static DATA_ITEMS = environment.DATA_BASE_END_POINT + 'items';
    static DATA_UPLOAD = environment.DATA_BASE_END_POINT + 'utils/upload';
    static DATA_ITEM_EXISTS = environment.DATA_BASE_END_POINT + 'utils/exists';
    static DATA_WORKERS = environment.DATA_BASE_END_POINT + 'workers';

    // Auth end-points
    static AUTH_LOGIN = environment.AUTH_BASE_END_POINT + 'login';
    static AUTH_LOGOUT = environment.AUTH_BASE_END_POINT + 'logout';
    static AUTH_IS_LOGGED = environment.AUTH_BASE_END_POINT + 'is-logged';
    static AUTH_REFRESH_TOKEN = environment.AUTH_BASE_END_POINT + 'refresh-token';
    static AUTH_REGISTER = environment.AUTH_BASE_END_POINT + 'register';
    static AUTH_UNREGISTER = environment.AUTH_BASE_END_POINT + 'unregister';
    static AUTH_CHECK_USERNAME = environment.AUTH_BASE_END_POINT + 'exists';
    static AUTH_PROFILE = environment.AUTH_BASE_END_POINT + 'data';
}
