import { TranslatePipe } from './translate.pipe';
import { CoreService } from '../../services/core.service';
import { TestBed } from '@angular/core/testing';
import { DictService } from '../../services/dict.service';

describe('Pipe: Translate', () => {

  let pipe: TranslatePipe;
  let coreService;
  let dictService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        CoreService,
        DictService
      ]
    });
    coreService = TestBed.inject(CoreService);
    dictService = TestBed.inject(DictService);
    pipe = new TranslatePipe(coreService, dictService);
  });

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should return pl version', () => {
    pipe.lang = 'pl';
    const result = pipe.transform({ key: 'required', value: true });
    expect(result).toBe(pipe.dict[pipe.lang].required);
  });

  it('should return de version', () => {
    pipe.lang = 'de';
    const result = pipe.transform({ key: 'email', value: true });
    expect(result).toBe(pipe.dict[pipe.lang].email);
  });

});
