import { Pipe, PipeTransform } from '@angular/core';
import { CoreService } from '../../services/core.service';
import { DictService } from '../../services/dict.service';

@Pipe({
  name: 'translate'
})
export class TranslatePipe implements PipeTransform {

  lang!: string;
  dict;

  constructor(
    coreService: CoreService,
    dictService: DictService
  ) {
    this.lang = coreService.lang;
    this.dict = dictService.dict;
  }

  transform({ key, value }: any): any {
    if (key
      && this.lang
      && this.dict[this.lang]
      && this.dict[this.lang][key]) {
      return this.dict[this.lang][key] + (typeof value != 'boolean' ? JSON.stringify(value) : '');
    } else {
      return JSON.stringify({ key, value });
    }
  }

}
