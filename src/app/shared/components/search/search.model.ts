export enum FieldTypes {
  INPUT_TEXT = 'INPUT_TEXT',
  INPUT_NUMBER = 'INPUT_NUMBER',
  SELECT = 'SELECT',
  IMAGE = 'IMAGE'
}

export interface SearchControlModel<T> {
  tag: string;
  name: T;
  value?: string;
  options?: string[];
}
