import { NgForm } from '@angular/forms';
import { AfterViewInit, Component, Input, Output, ViewChild, OnDestroy, EventEmitter } from '@angular/core';
import { SearchControlModel } from './search.model';
import { Subscription } from 'rxjs';
import { tap, filter, debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html'
})

export class SearchComponent implements AfterViewInit, OnDestroy {

  @Input() controls!: SearchControlModel<any>[];
  @Output() searchChange = new EventEmitter();
  @ViewChild('searchForm', { static: true }) searchForm!: NgForm;
  formInitValue!: {};
  formSubscription?: Subscription;

  ngAfterViewInit(): void {
    this.formSubscription = this.searchForm?.valueChanges?.pipe(
      tap((value: { [key: string]: any }) => {
        if (!this.formInitValue && Object.keys(value).length === this.controls.length) {
          this.formInitValue = { ...value };
        }
      }),
      filter(_ => !this.searchForm.pristine),
      debounceTime(500)
    )
      .subscribe((value) => this.searchChange.emit(value));
  }

  ngOnDestroy(): void {
    this.formSubscription?.unsubscribe();
  }

  clear() {
    this.searchForm.setValue(this.formInitValue);
  }
}
