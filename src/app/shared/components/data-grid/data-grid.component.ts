import { DataGridRowConfig } from './data-grid.models';
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Subject } from "rxjs";

@Component({
  selector: 'app-datagrid',
  templateUrl: './data-grid.component.html',
  styleUrls: ['data-grid.component.scss']
})

export class DataGridComponent {
  @Input() data: any[] = [];
  @Input() config!: DataGridRowConfig<any>[];
  @Input() itemAction!: Subject<any>;
  @Output() onAction = new EventEmitter();
}
