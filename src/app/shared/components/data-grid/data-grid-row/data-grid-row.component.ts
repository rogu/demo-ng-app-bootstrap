import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Subject } from "rxjs";
import { DataGridRowConfig } from '../data-grid.models';

@Component({
  selector: '[data-grid-row]',
  templateUrl: './data-grid-row.component.html'
})
export class DataGridRowComponent {
  @Input() data!: { [s: string]: string };
  @Input() config!: DataGridRowConfig<any>[];
  @Input() itemAction!: Subject<any>;
  @Output() onChange = new EventEmitter();
}
