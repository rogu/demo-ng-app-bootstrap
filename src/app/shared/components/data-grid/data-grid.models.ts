export interface DataGridRowConfig<T> {
  key?: T;
  type?: FieldTypes;
  header?: string;
  disabled?: boolean;
}

export enum FieldTypes {
  INPUT = 'input',
  IMAGE = 'img',
  BUTTON = 'button'
}

export interface GridAction {
  type: string,
  data: any
}
