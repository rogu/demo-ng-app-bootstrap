import { ModalDismissReasons, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { TemplateRef } from "@angular/core";
import { NgForm, FormGroup } from '@angular/forms';

export interface ModalModel {
  open(content: TemplateRef<any>): void;
  checkAndSend?(form: NgForm | FormGroup): void;
  send(result: any): void;
}

export class ModalBase implements ModalModel {

  openedModal!: NgbModalRef;

  constructor(private modalService: NgbModal) { }

  async open(content: TemplateRef<any>) {
    this.openedModal = this.modalService.open(content);

    const value = await this.openedModal.result;

    try {
      this.send(value);
    } catch (error) {
      console.warn(this.getDismissReason(error));
    }
  }

  checkAndSend(form: FormGroup | NgForm) {
    if (form.valid) {
      this.openedModal.close(form.value);
    } else {
      console.log('form invalid');
    }
  }

  send(result: { [s: string]: string }) {
    throw new Error('you should override go method');
  }

  getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}
