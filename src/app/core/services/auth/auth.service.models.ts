export interface AuthServiceModel {
  isLogged(): void;
  logIn(value: { username: string, password: string }): void;
  logOut(): void;
}

export interface AuthDataModel {
  username: string;
  password: string;
}

export interface ResponseAuth {
  message: string;
  error: string;
  accessToken: string;
  refreshToken: string;
}
