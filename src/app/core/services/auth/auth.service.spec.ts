import { Api } from './../../../shared/utils/api';
import { RouterTestingModule } from '@angular/router/testing';
import { inject, TestBed } from '@angular/core/testing';
import { AuthService } from './auth.service';
import { HttpClientTestingModule, HttpTestingController } from "@angular/common/http/testing";

describe('Auth service', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule
      ],
      providers: [
        AuthService
      ]
    });
  });

  it('should return true when login is ok', inject([AuthService], (authService: any) => {
    const http = TestBed.get(HttpTestingController);

    authService
      .logIn({})
      .subscribe((data: any) => {
        expect(data.ok).toBeTruthy()
      });

    const req = http.expectOne(Api.AUTH_LOGIN);
    expect(req.request.method).toEqual('POST');
    req.flush({ ok: true });
    http.verify();
  }));

});
