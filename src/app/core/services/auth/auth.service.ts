import { Api } from './../../../shared/utils/api';
import { CoreService } from '../../../shared/services/core.service';
import { Injectable } from '@angular/core';
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { AuthServiceModel, AuthDataModel, ResponseAuth } from './auth.service.models';

@Injectable({ providedIn: 'root' })
export class AuthService implements AuthServiceModel {

  constructor(
    private http: HttpClient,
    private coreService: CoreService
  ) {
    this.isLogged()
      .subscribe(({ error, message }: ResponseAuth) => {
        this.coreService.setAccess(error, message, error ? false : true);
      });

  }
  isLogged() {
    return this.http.get<ResponseAuth>(Api.AUTH_IS_LOGGED);
  }

  logIn(value: AuthDataModel): Observable<any> {
    return this.http.post(Api.AUTH_LOGIN, value);
  }

  logOut() {
    localStorage.removeItem('token');
    this.coreService.setAccess('', 'you are logged out', false);
  }

  refreshToken(): Observable<ResponseAuth> {
    return this.http.get<ResponseAuth>(Api.AUTH_REFRESH_TOKEN);
  }

}
