import { of, iif } from 'rxjs';
import { ResponseData } from '../../../shared/services/http.models';
import { map, tap, switchMap } from 'rxjs/operators';
import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { CanActivate, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  canActivate(): Observable<UrlTree | boolean> {
    return this.authService.isLogged()
      .pipe(
        map((resp: any) => !!resp.user),
        switchMap((loggedin: boolean) => iif(() =>
          loggedin,
          of(true),
          of(this.router.createUrlTree(['/home'])).pipe(tap(() => {
            alert('you shall not pass; try login');
            this.authService.logOut();
          }))
        ))
      )
  }

}
