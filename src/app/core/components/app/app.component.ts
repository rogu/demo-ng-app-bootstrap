import { AuthService } from './../../services/auth/auth.service';
import { CoreService } from '../../../shared/services/core.service';
import { Component } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  unsaved$: BehaviorSubject<boolean> = this.coreService.unsaved$;
  access$: BehaviorSubject<boolean> = this.coreService.access$;
  logout: Function = this.authService.logOut;

  constructor(
    private coreService: CoreService,
    public authService: AuthService
  ) { }

  get serverInfo$() {
    return this.coreService.serverInfo$;
  }

}
