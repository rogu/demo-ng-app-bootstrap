import { CoreService } from './../../../shared/services/core.service';
import { switchMap, delay, map } from 'rxjs/operators';
import { Component, OnInit, Input } from '@angular/core';
import { Router, RouteConfigLoadStart } from '@angular/router';
import { BehaviorSubject, iif, merge, of, Observable } from 'rxjs';

@Component({
  selector: 'app-spinner',
  template: `
  <div [ngClass]="{ wrapper: show$ | async }" *ngIf="show$|async">
    <div class="m-auto">
        <div *ngFor="let d of ['.5','.01','.3']; let i =index"
            [style.animation-delay] = "d + 's'"
            [style.vertical-align.px]="(i === 2 ? -1 : i) * 10"
            [style.width.px]="(i+1) * 10"
            [style.height.px]="(i+1) * 10"
            class="spinner-grow col-auto text-info"
            role="status">
        </div>
    </div>
  </div>
  `,
  styles: [`
  .wrapper {
    position: absolute;
    width: 100%;
    height: 100%;
    background-color: rgba(0,0,0,.6);
    left:0;
    z-index: 100;
    top:0;
    transition: background-color 200ms linear;
    display: flex;
  }
  `]
})
export class SpinnerComponent implements OnInit {
  @Input() loading!: boolean | null;
  show$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  loading$: Observable<boolean> = this.coreService.unsaved$;

  constructor(
    private router: Router,
    private coreService: CoreService
  ) { }

  ngOnInit() {
    merge(
      this.loading$,
      this.router.events.pipe(map((ev) => ev instanceof RouteConfigLoadStart))
    )
      .pipe(
        switchMap((ev: any) => iif(() => ev, of(true), of(false).pipe(delay(500))))
      )
      .subscribe(ev => this.show$.next(ev));
  }

}
