export const environment = {
  production: false,
  DATA_BASE_END_POINT: 'http://localhost:3000/',
  AUTH_BASE_END_POINT: 'https://auth.debugger.pl/'
};
