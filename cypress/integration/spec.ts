describe('My First Test', () => {
  it('Visits the initial project page', () => {
    cy.visit('/')
    cy.contains('Szkolenie Angular')
    cy.contains('Demo aplikacji')
  })
  it('should display proper version', () => {
    cy.visit('/');
    cy.task('readFileMaybe', 'package.json').then((textOrNull: any) => {
      const version = JSON.parse(textOrNull)?.version;
      cy.get('main :nth-child(4) > .card').then((el) => {
        expect(el.text().trim()).eq(`v. ${version}`);
      })
    })
  })
})
