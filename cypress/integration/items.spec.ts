describe('items view', () => {

  let itemMock;

  before(() => {
    cy.visit('login');
    cy.fixture('item').then((item) => itemMock = { ...item.data, title: item.data.title + Date.now() });
    cy.fixture('auth').then((auth) =>
      cy.login(auth.username, auth.password).then((btnLogin) => cy.wrap(btnLogin).should('not.exist')));
  })

  beforeEach(() => {
    cy.restoreLocalStorage();
    cy.visit('items');
  });

  afterEach(() => {
    cy.saveLocalStorage();
  });

  it('should add & remove item', () => {

    /* add item */
    cy.contains('add item').click();
    cy.intercept('https://api.debugger.pl/utils/exists?title=' + itemMock.title).as('respTitle');
    cy.intercept('https://api.debugger.pl/utils/upload').as('respFile');
    cy.fillItemForm(itemMock);
    cy.wait(['@respTitle', '@respFile']).then(([{ response: { body: { message } } }]) => {
      expect(message).eq('title ok');
      cy.get('#add-item-form img').should('exist');
      cy.get('#add-item-form').find('.btn').click();
      cy.get('#add-item-form').should('not.exist');
    })

    /* search & remove added item */
    cy.get('#search-by-title').type(itemMock.title);
    cy.get('app-datagrid td').contains(itemMock.title).should('exist');
    cy.get('app-datagrid tbody').find('tr').should('have.length', 1)
    cy.get('app-datagrid td').contains('remove').click();
    cy.get('app-datagrid td').contains(itemMock.title).should('not.exist');
  })

  context('should show', () => {
    it('3 errors when entire form is empty', () => {
      cy.get('modal-add button').contains('add item').click();
      cy.get('#add-item-form').find('button').click();
      cy.get('app-errors div').should('have.length', 3);
    })

    it('1 errors when title field is empty', () => {
      cy.get('modal-add button').contains('add item').click();
      cy.fillItemForm({ ...itemMock, title: ' ' });
      cy.get('#add-item-form').find('button').click();
      cy.get('app-errors div').should('have.length', 1);
    })
  })

  it('should display 2 items', () => {
    // req with mock
    cy.intercept('**/items?**', { fixture: 'items.json' }).as('items');
    cy.visit('items');
    cy.wait('@items').then((resp) => {
      cy.get('tbody').find('tr').should('have.length', 2);
    })
  })
})
