describe('workers', () => {

  const uniqueName = "Joe" + Date.now();

  before(() => {
    cy.visit('/workers');
  })

  it('should add worker', () => {
    cy.contains('add worker').click();
    cy.get('#add-worker-form input[placeholder=name]').type(uniqueName);
    cy.get('#add-worker-form input[placeholder=phone]').type("1234");
    cy.get('#add-worker-form select').select('support');
    cy.get('#add-worker-form').find('.btn').click();
    cy.get('#add-worker-form').should('not.exist');

    /* search added */
    cy.get('#search-by-name').type(uniqueName);
    cy.get('tbody').find('tr').should('have.length', 1);
  })
})
