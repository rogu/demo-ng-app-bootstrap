import { of } from 'rxjs';
import { ItemsComponent } from '../../src/app/containers/items/components/items/items.component';
describe.skip('items', () => {

  before(() => {

    cy.visit('/items');

    cy.fixture('items').then((mockData) => {
      cy.window()
        .should('have.property', 'ItemsComponent')
        .then((comp: any) => {
          (comp as ItemsComponent).items$ = of<any>({
            total: 1,
            data: [
              mockData.data[0]
            ]
          })
        })
        .wait(2000)
    })

    cy.window()
      .should('have.property', 'appRef')

  })

  it('should make req & display item after invoked "more" function', () => {

    cy.intercept('**/items?**', { fixture: 'items.json' }).as('items');
    cy.intercept('**/items/1', { fixture: 'item.json' }).as('item');

    cy.visit('items');

    cy.wait('@items').then(({ response: { body: { data } } }) => {
      cy.window()
        .its('ItemsComponent')
        .invoke('more', data[0])
    })

    cy.wait('@item').then(({ response: { body: { data } } }) => {
      cy.window()
        .its('appRef')
        .invoke('tick')
      cy.contains(data.title)
    })

  })

})
