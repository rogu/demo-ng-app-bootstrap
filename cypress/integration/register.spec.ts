describe('register view', () => {

  const username = 'admin@localhost';

  beforeEach(() => {
    cy.visit('/register');
  })

  context('register user should show error', () => {
    it('when user exists', () => {
      cy.intercept('https://auth.debugger.pl/exists?username=' + username).as('resp');

      cy
        .get('#register-form input[name=username]')
        .type(username)
        .parent()
        .find('app-errors')
        .should('contain.text', 'exists')

      cy.wait('@resp').then((obj: any) => {
        expect(obj.response.body.error).equal('username exists');
      })
    })

    it('when passwords are not equals', () => {
      cy
        .get('#register-form input[name=password]')
        .type('Admin1');

      cy
        .get('#register-form input[name=confirmPassword]')
        .type('Admin2');

      cy
        .get('#register-form button').contains('send me').click();

      cy
        .get('#register-form input[name=password]')
        .parent()
        .parent()
        .parent()
        .find('app-errors')
        .should('contain.text', 'wyrażenia nie są zgodne')
    })
  })

})
