Cypress.Commands.add('fillItemForm', ({ title, price, category }: any) => {
  cy.get('#add-item-form input[placeholder=title]').type(title);
  cy.get('#add-item-form input[placeholder=price]').type(price);
  cy.get('#add-item-form select').select(category);
  cy.get('#add-item-form file-upload input[type=file]').attachFile('in.png');
  return cy;
})

